# -*- coding: utf-8 -*-
# /######################################################################################
#
#    Klystron Technologies
#    Copyright (C) 2004-TODAY Klystron Technologies Pvt Ltd.(<http://klystrontech.com/>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# /######################################################################################

from odoo import models, fields, api, _
import logging
from odoo.tools.translate import _
from odoo import http
from odoo.exceptions import UserError, ValidationError, AccessError, UserError

_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    user_id = fields.Many2one('res.users', string='Account Manager',
                              help='The internal user that is in charge of communicating with this contact if any.')
    mode_of_cust = fields.Selection(string="Mode of Customer", selection=[('new', 'New'), ('focus', 'Focus')])
    is_vat_applicable = fields.Boolean("VAT Applicable")
    trn_no = fields.Char(string="TRN No")
    type_of = fields.Selection(string="Type", selection=[('customer', 'Customer'), ('prospect', 'Prospect')])

    is_reg_req = fields.Boolean("Is registration required ?")
    url = fields.Char(string="URL")
    login = fields.Char(string="Login")
    password = fields.Char(string="Password")
    expired_date = fields.Char(string="Expired Date")
    reg_email = fields.Char(string="Registered Email")

    credit_limit = fields.Float(string="Credit Limit")

    kg_industry_id = fields.Many2one("kg.industry", string="Industry")

    payment_term_id = fields.Many2one('account.payment.term', string='Payment Terms')


class UtmMixin(models.AbstractModel):
    _inherit = 'utm.mixin'
    source_id = fields.Many2one('utm.source', 'Source',
                                help="This is the source of the link Ex:Search Engine, another domain,or name of email list")


class Lead(models.Model):
    _inherit = 'crm.lead'

    lead_status = fields.Selection(string="Lead Status",
                                   selection=[('job_in_hand', 'Job In Hand'), ('tender', 'Tender')])
    business_id = fields.Many2one("kg.business.type", string="Type Of Business")
    material_id = fields.Many2one("material.requirement", string="Material Requirement")
    planned_revenue = fields.Float('Deal Amount', track_visibility='always')

    @api.multi
    def _compute_regret_stage(self):
        for file in self:
            if file.stage_id.name == 'Regret':
                file.regret_stage_boolean = True
            # else:
            #     file.regret_stage_boolean = False

    @api.multi
    def _compute_qualified_stage(self):
        for obj in self:
            if obj.stage_id.name == 'Qualified':
                obj.qualified_stage_boolean = True
            # else:
            #     obj.qualified_stage_boolean = False

    @api.multi
    def _compute_design_stage(self):
        for des in self:
            if des.stage_id.name == 'Design And Estimation':
                des.design_stage_boolean = True
            # else:
            #     des.design_stage_boolean = False

    @api.multi
    def _compute_proposed_stage(self):
        for file in self:
            if file.stage_id.name == 'Proposed':
                file.proposed_stage_boolean = True
            # else:
            #     file.proposed_stage_boolean = False

    @api.multi
    def _compute_won_stage(self):
        for file in self:
            if file.stage_id.name == 'Won':
                file.won_stage_boolean = True
            # else:
            #     file.won_stage_boolean = False

    won_stage_boolean = fields.Boolean("Design & Estimation", compute="_compute_won_stage")
    proposed_stage_boolean = fields.Boolean("Design & Estimation", compute="_compute_proposed_stage")
    design_stage_boolean = fields.Boolean("Design & Estimation", compute="_compute_design_stage")
    qualified_stage_boolean = fields.Boolean("Regret Stage", compute="_compute_qualified_stage")
    regret_stage_boolean = fields.Boolean("Regret Stage", compute="_compute_regret_stage")

    regret_stage_reason = fields.Char(string="Reason (Regret Stage)")
    project = fields.Selection(string="Project Type", selection=[('long_term', 'Construction Long Term'),
                                                                 ('short_term', 'Construction Short Term'),
                                                                 ('trading', 'Trading'),
                                                                 ('excisting_properties', 'Existing properties'),
                                                                 ('fit_out', 'Fit Out'),
                                                                 ('upgradation', 'Upgradation')])
    end_user = fields.Many2one("end.user", string="End User")
    main_contractor_id = fields.Many2one("main.contractor", string="Main Contractor")
    consultant_id = fields.Many2one("kg.consultant", string="Consultant")
    mep_contractor_id = fields.Many2one("mep.contractor", string="MEP Contractor")
    won_date = fields.Date(string="Won Date")
    quote_due_date = fields.Date(string="Quote Submission Due Date")

    target_ids = fields.One2many("salesman.target", "lead_id", string="Target Ids")


class SalesmanTarget(models.Model):
    _name = 'salesman.target'
    _rec_name = 'salesman_id'

    salesman_id = fields.Many2one("res.users", string="Salesperson")
    target_amt = fields.Float(string="Target Amount")
    date_from = fields.Date(string="Date From")
    date_to = fields.Date(string="Date To")

    lead_id = fields.Many2one("crm.lead", string="Lead Id")


class DailyActivities(models.Model):
    _name = 'daily.activities'
    _rec_name = 'salesman_id'

    salesman_id = fields.Many2one("res.users", string="Salesperson")
    activity = fields.Selection(string="Activity",
                                selection=[('customer_meeting', 'Customer Meeting'), ('travel', 'Travel'),
                                           ('office_work', 'Office Work'), ('internal_meeting', 'Internal Meeting'),
                                           ('training_seminar', 'Training/Seminar'),
                                           ('project_meeting', 'Project Meeting')])
    lead_id = fields.Many2one("crm.lead", string="Lead")
    project_desc = fields.Text(string="Project Description")
    hour = fields.Float(string="Hour")


class KgBusinessType(models.Model):
    _name = 'kg.business.type'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    internal_note = fields.Text(string="Internal Notes")


class KgIndustry(models.Model):
    _name = 'kg.industry'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    internal_note = fields.Text(string="Internal Notes")


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    material_cost = fields.Float(string="Material Cost")
    installation_cost = fields.Float(string="Installation")

    material_budget = fields.Float(string="Material Budget")
    miscellaneous_budget = fields.Float(string="Miscellaneous Budget")
    man_hours = fields.Float(string="Man hours")

    subject = fields.Char(string="Subject")

    client_order_ref = fields.Char(string='LPO No', copy=False)
    lpo_date = fields.Date(string="LPO Date")

    state = fields.Selection([('draft', 'Quotation'),
                              ('sent', 'Quotation Sent'),
                              ('confirm', 'Confirmation'),
                              ('sale', 'Sales Order'),
                              ('done', 'Locked'),
                              ('cancel', 'Cancelled'), ], string='Status', readonly=True, copy=False, index=True,
                             track_visibility='onchange', default='draft')

    @api.multi
    def confirmed(self):
        self.write({'state': 'confirm'})
        return True


class ProductProduct(models.Model):
    _inherit = 'product.product'

    default_code = fields.Char('Part Code', index=True)
    brand_id = fields.Many2one("kg.brand", string="Brand")


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    default_code = fields.Char(
        'Part Code', compute='_compute_default_code',
        inverse='_set_default_code', store=True)
    brand_id = fields.Many2one("kg.brand", string="Brand")


class MaterialRequirement(models.Model):
    _name = 'material.requirement'
    _rec_name = 'name'
    _description = 'Material Requirement'

    name = fields.Char(string="Name of Material")


class EndUser(models.Model):
    _name = 'end.user'
    _rec_name = 'name'
    _description = 'end.user'

    name = fields.Char(string="Name")
    internal_note = fields.Text(string="Internal Notes")


class MainContarctor(models.Model):
    _name = 'main.contractor'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    internal_note = fields.Text(string="Internal Notes")


class Consultant(models.Model):
    _name = 'kg.consultant'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    internal_note = fields.Text(string="Internal Notes")


class MepContactor(models.Model):
    _name = 'mep.contractor'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    internal_note = fields.Text(string="Internal Notes")


class ProjectProject(models.Model):
    _inherit = 'project.project'

    kg_type = fields.Selection(string="Type", selection=[('amc', 'AMC'), ('normal', 'Normal')])

    start_date = fields.Datetime(string="Start Date")
    scheduled_end_date = fields.Datetime(string="Scheduled End Date")
    actual_end_date = fields.Datetime(string="Actual End Date")
    invoice_date = fields.Datetime(string="Next Invoice Date")

    payment_term_id = fields.Many2one('account.payment.term', string='Payment Terms')
    next_payment_date = fields.Date(string="Next Payment Date")

    @api.depends('name')
    def _compute_confirmation_date(self):
        for file in self:
            if file.name:
                sale_order = self.env['sale.order'].search([('name', '=', file.name)])
                if sale_order:
                    file.confirmation_date = sale_order.confirmation_date
                    file.sale_order_no = sale_order.name

    confirmation_date = fields.Datetime(string='Sale Order Date', compute='_compute_confirmation_date')
    sale_order_no = fields.Char(string="Sale Order No", compute='_compute_confirmation_date')


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    project_id = fields.Many2one("project.project", string="Project")
    # payment_term_id = fields.Many2one('account.payment.term', string='Payment Terms')
    # next_payment_date = fields.Date(string="Next Payment Date")


class ProjectTask(models.Model):
    _inherit = 'project.task'

    qty = fields.Char(string="Qty")


class HrContract(models.Model):
    _inherit = 'hr.contract'

    @api.depends('wage', 'od_allowance_rule_line_ids.amt')
    def _get_total_wage(self):
        lines = self.od_allowance_rule_line_ids

        tot = 0
        for line in lines:
            tot = tot + line.amt

        self.od_total_wage = tot + self.wage

    @api.multi
    @api.one
    @api.constrains('employee_id', 'state')
    def _check_constriant(self):
        state = self.state
        employee_id = self.employee_id and self.employee_id.id
        contract = self.env['hr.contract'].search(
            [('employee_id', '=', employee_id), ('state', 'in', ('open', 'pending'))])
        if len(contract) > 1:
            raise UserError(_('Only One Active Contract At a Time.'))

    kg_working_hours = fields.Float(string='Working Hour', default=9)
    od_total_wage = fields.Float(string="Total Wage", compute='_get_total_wage')
    od_allowance_rule_line_ids = fields.One2many('od.allowance.rule.line', 'contract_id', 'Rule Lines')
    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id)
    kg_to_name = fields.Char(string="Salary Certificate To")
    kg_to_address = fields.Char(string="Salary Certificate Address To")

    kg_to_name_arabic = fields.Char(string="Salary Certificate To")
    kg_to_address_arabic = fields.Char(string="Salary Certificate Address To")

    kg_contract_date = fields.Date(string="Contract Print Date", default=fields.Date.today())


class odallowance_rule_line(models.Model):
    _name = 'od.allowance.rule.line'
    _description = 'Contract Allowance Rule Line'

    contract_id = fields.Many2one('hr.contract', 'Contract ID', ondelete='cascade')

    deduct_in_unpaid_leave = fields.Boolean('Deduct(Unpaid Leave)', default=True)
    rule_type = fields.Many2one('hr.salary.rule', 'Allowance Rule')
    code = fields.Char(string='Code')
    amt = fields.Float('Amount')


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    asset_ids = fields.One2many("hr.employee.asset", "employee_id", string="Asset ID")
    emp_grade = fields.Selection(string="Grade",
                                 selection=[('a', 'A'), ('b', 'B'), ('c', 'C'), ('d', 'D'), ('e', 'E'), ('f', 'F'),
                                            ('g', 'G'),
                                            ('h', 'H')])
    ticket_eligibility = fields.Selection(string="Ticket Eligibility", selection=[('once_in_a_year', 'Once in a year'),
                                                                                  ('once_in_every_two_year',
                                                                                   'Once in every two year')])
    working_hours = fields.Float(string="Working Hours")


class HrEmployeeAsset(models.Model):
    _name = 'hr.employee.asset'
    _rec_name = 'name'
    _description = 'HR Employee Asset'

    name = fields.Char(string="Assets")
    employee_id = fields.Many2one("hr.employee", string="Employee ID")
