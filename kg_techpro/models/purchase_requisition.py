# -*- coding: utf-8 -*-
# /######################################################################################
#
#    Klystron Technologies
#    Copyright (C) 2004-TODAY Klystron Technologies Pvt Ltd.(<http://klystrontech.com/>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# /######################################################################################

from odoo import models, fields, api, _


class KgPurchaseRequisition(models.Model):
    _name = 'kg.purchase.requisition'
    _rec_name = 'project_id'

    @api.multi
    def approve(self):
        self.write({'state': 'done'})
        return True

    @api.multi
    def reject(self):
        self.write({'state': 'cancel'})
        return True

    partner_id = fields.Many2one("res.partner", string="Customer", related="project_id.partner_id")
    project_id = fields.Many2one("account.analytic.account", string="Project")
    requested_date = fields.Date(string="Requested Date")
    required_date = fields.Date(string="Required Date")
    delivery_id = fields.Text(string="Delivery Place")
    state = fields.Selection(string="State", selection=[('new', 'New'), ('done', 'Done'), ('cancel', 'Cancel')],
                             default='new')

    purchase_req_ids = fields.One2many("kg.purchase.requisition.line", "purchase_req_id", string="Purchase Req IDS")


class KgPurchaseRequisitionLine(models.Model):
    _name = 'kg.purchase.requisition.line'

    default_code = fields.Char('Part Code', related='product_id.default_code')
    product_id = fields.Many2one("product.product", string="Product")
    brand_id = fields.Many2one("kg.brand", string="Brand", related='product_id.brand_id')
    qty = fields.Integer(string="Qty")
    uom_id = fields.Many2one("product.uom", string="Unit Of Measure", related='product_id.uom_id')

    purchase_req_id = fields.Many2one("kg.purchase.requisition", string="Purchase Req ID")


class KgBrand(models.Model):
    _name = 'kg.brand'
    _rec_name = 'name'

    name = fields.Char(string="Brand")
    code = fields.Char(string="Code")


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def purchase_manager_confirm(self):
        self.write({'state': 'purchase_manager'})
        return True

    @api.multi
    def finance_manager_confirm(self):
        self.write({'state': 'finance_manager'})
        return True

    @api.onchange('partner_id')
    def _onchange_vendor_payment_term_id(self):
        for obj in self:
            if obj.partner_id:
                obj.vendor_payment_term_id = obj.partner_id.payment_term_id.id

    state = fields.Selection([
        ('draft', 'RFQ'),
        ('sent', 'RFQ Sent'),
        ('to approve', 'To Approve'),
        ('finance_manager', 'Finance Manager Approved'),
        ('purchase_manager', 'Purchase Manager Approved'),
        ('purchase', 'Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled')
    ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility='onchange')

    contact_id = fields.Many2one("res.partner", string="Contact Person")
    contact_phone = fields.Char(string="Phone", related='contact_id.phone')
    contact_mobile = fields.Char(string="Mobile", related='contact_id.mobile')

    vendor_payment_term_id = fields.Many2one('account.payment.term', string='Payment Terms')
